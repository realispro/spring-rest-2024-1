package spring.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ComponentScan("spring")
public class TravelConfig {


    @Bean("travelName")
    String travelName(){
        return "Summer Holiday 2024";
    }

    @Bean
    List<String> meals(@Value("#{'${meals.menu}'.split(',')}") List<String> menu){
        return menu;
    }

}
