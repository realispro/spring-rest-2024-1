package spring.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import spring.Person;
import spring.Transportation;
import spring.config.Land;

@Component("bus")
@Qualifier("land")
@Land
public class Bus implements Transportation {
    @Override
    public void transport(Person p) {
        System.out.println("person " + p + " is being transported by BUS");
    }

}
