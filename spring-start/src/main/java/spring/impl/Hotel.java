package spring.impl;


import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;


import java.util.ArrayList;
import java.util.List;

@Component
@Lazy(false)
public class Hotel implements Accomodation {

    @Value("#{'${meals.gratis:figa z makiem}'.toUpperCase()}")
    private String gratis;

    private List<String> meals;

    @PostConstruct
    public void postConstruct(){
        meals.add(gratis);
    }

    @PreDestroy
    public void preDestroy(){
        System.out.println("destroying hotel :/");
    }


    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }

    @Autowired
    public void setMeals(@Qualifier("meals") List<String> meals) {
        this.meals = new ArrayList<>(meals);
    }
}
