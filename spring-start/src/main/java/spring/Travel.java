package spring;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import spring.config.Land;

@Component
public class Travel {

    private final String name;
    private final Transportation transportation;
    private final Accomodation accomodation;

    public Travel(String name, @Land Transportation transportation, Accomodation accomodation) {
        this.name = name;
        this.transportation = transportation;
        this.accomodation = accomodation;
    }


    public void travel(Person p) {
        System.out.println("started travel " + name + " for a person " + p);
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
