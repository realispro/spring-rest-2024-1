package vod.client;

import lombok.Data;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import vod.dto.MovieDto;

import java.util.Arrays;
import java.util.List;

public class VodClient {

    public static void main(String[] args) {
        System.out.println("Let's browse movies list!");

        String serviceUrl = "http://localhost:8080/vod";

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<MovieDto[]> entity = restTemplate.exchange(
                serviceUrl + "/movies",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                MovieDto[].class
        );

        System.out.println("movies list: " + Arrays.toString(entity.getBody()));
    }


    @Data
    static class MovieData {
        private int id;
        private String title;
        private String poster;
        private int directorId;
    }
}
