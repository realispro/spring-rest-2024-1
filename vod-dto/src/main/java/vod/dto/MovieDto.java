package vod.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MovieDto {

    private int id;
    @NotNull
    private String title;
    @Size(min=5)
    @URL
    private String poster;
    private int directorId;

}
