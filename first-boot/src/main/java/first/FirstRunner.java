package first;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
//@Order(1)
public class FirstRunner implements CommandLineRunner {

    private final FirstComponent firstComponent;
    @Override
    public void run(String... args) throws Exception {
        log.info("starting first runner");
        log.info("greetings: {}", firstComponent.sayHi());
    }
}
