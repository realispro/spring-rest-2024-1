package first;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FirstComponent {

    @PostConstruct
    public void postConstruct(){
        log.info("first component successfully constructed");
    }

    public String sayHi(){
        return "Hi Universe!";
    }
}
