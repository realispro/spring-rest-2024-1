package first;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class FirstController {

    private final FirstComponent firstComponent;

    @GetMapping("/hello")
    String sayHi(){
        return firstComponent.sayHi();
    }

}
