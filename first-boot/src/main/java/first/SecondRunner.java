package first;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Slf4j
//@Order(2)
@DependsOn("firstRunner")
public class SecondRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        log.info("starting second runner");
    }
}
