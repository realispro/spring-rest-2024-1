package vod.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import vod.web.TimeWindowInterceptor;

@Configuration
@ComponentScan("vod")
@RequiredArgsConstructor
public class VodConfig implements WebMvcConfigurer {

    private final TimeWindowInterceptor timeWindowInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(timeWindowInterceptor);
    }
}
