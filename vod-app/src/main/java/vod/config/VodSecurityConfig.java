package vod.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class VodSecurityConfig {

  /*  @Bean
    UserDetailsService userDetailsService(){

        UserDetails user1 = User.withUsername("user1")
                .password("user1")
                .authorities("ROLE_USER")
                .build();

        UserDetails user2 = User.withUsername("user2")
                .password("user2")
                .authorities("ROLE_ADMIN")
                .build();

        return new InMemoryUserDetailsManager(user1, user2);
    }*/

 /*   @Bean
    PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }*/


    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        // diff
        http.csrf(csrf->csrf.disable());

        // authorize
        http.authorizeHttpRequests(request-> request
                .requestMatchers(HttpMethod.POST, "/movies").hasRole("ADMIN")
                .requestMatchers(HttpMethod.GET, "/movies").authenticated()
                .anyRequest().permitAll()
        );

        // auth method
        http.oauth2ResourceServer(oauth2->oauth2.jwt(Customizer.withDefaults()));
                //.httpBasic(Customizer.withDefaults());

        return http.build();
    }

    @Bean
    JwtAuthenticationConverter jwtAuthenticationConverter(){
        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthoritiesClaimName("realm_access.roles");
        grantedAuthoritiesConverter.setAuthorityPrefix("");

        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return converter;
    }

}
