/*
package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import vod.model.Cinema;
import vod.model.Movie;
import vod.repository.CinemaDao;

import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnProperty(name = "vod.dao.type", havingValue = "jpa")
public class JpaCinemaDao implements CinemaDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Cinema> findAll() {
        // SQL -> HQL -> JPQL
        return entityManager.createQuery("select cinema from Cinema cinema").getResultList();
    }

    @Override
    public Optional<Cinema> findById(int id) {
        return Optional.ofNullable(
                entityManager.find(Cinema.class, id)
        );
    }

    @Override
    public List<Cinema> findByMovie(Movie m) {
        return entityManager
                .createQuery("select cinema from Cinema cinema inner join cinema.movies movie where movie=:m")
                .setParameter("m", m)
                .getResultList();
    }

    @Override
    public Cinema save(Cinema c) {
        entityManager.persist(c);
        return c;
    }
}
*/
