package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseStatus;
import vod.web.validation.MovieDtoValidator;

@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class VodAdvice {

    private final MovieDtoValidator validator;

    @InitBinder("movieDto")
    void initBinder(WebDataBinder binder){
        binder.addValidators(validator);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    void handleIllegalArgumentException(IllegalArgumentException e){
        log.error("handling exception", e);
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<String> handleException(Exception e){
        log.error("handling exception", e);
        return ResponseEntity.status(HttpStatus.PAYLOAD_TOO_LARGE).body(e.getMessage());
    }
}
