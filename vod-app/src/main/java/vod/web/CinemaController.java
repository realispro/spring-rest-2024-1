package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.CinemaConverter;
import vod.dto.CinemaDto;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequiredArgsConstructor
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas")   // /cinemas?name=Kosmos&param1=value1&param2=value2
    List<CinemaDto> getCinemas(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam Map<String, String> requestParams,
            @RequestHeader(value = "foo", required = false) String fooHeader,
            @RequestHeader Map<String, String> requestHeaders,
            @CookieValue(value = "fooCookie", required = false) String cookieParam
            ) {
        log.info("about to retrieve cinemas");

        log.info("request param name: {}", name);
        log.info("request params: {}", requestParams);
        log.info("request header foo: {}", fooHeader);
        log.info("request headers: {}", requestHeaders);
        log.info("Cookie param: {}", cookieParam);

        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info("found {} cinemas", cinemas.size());
        List<CinemaDto> dtos = cinemas.stream()
                .map(cinema -> CinemaConverter.toDto(cinema))
                .toList();
        return dtos;
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDto> getCinema(@PathVariable("cinemaId") int cinemaId) {
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if (cinema != null) {
            CinemaDto dto = CinemaConverter.toDto(cinema);
            return ResponseEntity.ok(dto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDto>> getCinemasShowingMovie(@PathVariable("movieId") int movieId) {
        log.info("about to retrieve cinemas showing movie {}", movieId);

        Movie movie = movieService.getMovieById(movieId);
        if (movie != null) {
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            log.info("there is {} cinemas showing movie {}", cinemas.size(), movie.getTitle());
            return ResponseEntity.ok(
                    cinemas.stream()
                            .map(CinemaConverter::toDto)
                            .toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }


}
