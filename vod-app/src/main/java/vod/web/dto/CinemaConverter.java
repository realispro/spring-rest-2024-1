package vod.web.dto;

import vod.dto.CinemaDto;
import vod.model.Cinema;

public class CinemaConverter {

    public static CinemaDto toDto(Cinema cinema){
        return CinemaDto.builder()
                .id(cinema.getId())
                .name(cinema.getName())
                .logo(cinema.getLogo())
                .build();
    }

}
