package vod.web.dto;

import vod.dto.MovieDto;
import vod.model.Director;
import vod.model.Movie;

public class MovieConverter {

    public static MovieDto toDto(Movie movie){
        return MovieDto.builder()
                .id(movie.getId())
                .title(movie.getTitle())
                .poster(movie.getPoster())
                .directorId(movie.getDirector().getId())
                .build();
    }

    public static Movie fromDto(MovieDto dto){
        Movie movie = new Movie();
        movie.setId(dto.getId());
        movie.setTitle(dto.getTitle());
        movie.setPoster(dto.getPoster());

        Director director = new Director();
        director.setId(dto.getDirectorId());
        movie.setDirector(director);

        return movie;
    }
}
