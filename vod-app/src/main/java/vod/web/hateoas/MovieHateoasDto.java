package vod.web.hateoas;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class MovieHateoasDto extends RepresentationModel<MovieHateoasDto> {

    private int id;
    private String title;
    private String poster;
}
