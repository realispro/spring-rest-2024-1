package vod.web.hateoas;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vod.model.Movie;
import vod.service.MovieService;
import vod.web.CinemaController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/hateoas")
public class MovieHateoasController {

    private final MovieService movieService;

    @GetMapping("/movies/{movieId}")
    MovieHateoasDto getMovieById(@PathVariable("movieId") int movieId){
        log.info("about to retrieve movie {}", movieId);

        Movie movie = movieService.getMovieById(movieId);

        MovieHateoasDto dto = new MovieHateoasDto();
        dto.setId(movieId);
        dto.setTitle(movie.getTitle());
        dto.setPoster(movie.getPoster());

        // link
        Link cinemasLink = WebMvcLinkBuilder.linkTo(CinemaController.class)
                .slash("movies")
                .slash(movieId)
                .slash("cinemas")
                .withRel("cinemas");
        Link directorLink = WebMvcLinkBuilder.linkTo(CinemaController.class)
                .slash("directors")
                .slash(movie.getDirector().getId())
                .withRel("director");
        Link selfLink = WebMvcLinkBuilder.linkTo(MovieHateoasController.class)
                .slash("/movies")
                .slash(movieId)
                .withSelfRel();

        dto.add(cinemasLink, directorLink, selfLink);

        return dto;
    }


}
