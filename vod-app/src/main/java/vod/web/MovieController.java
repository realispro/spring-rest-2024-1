package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.MovieConverter;
import vod.dto.MovieDto;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@RestController
@Slf4j
@RequiredArgsConstructor
public class MovieController {

    private final MovieService movieService;
    private final CinemaService cinemaService;

    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;


    @GetMapping(value = "/movies"/*, produces = {MediaType.APPLICATION_JSON_VALUE}*/)
    List<MovieDto> getMovies() {
        log.info("about to get all movies ....");

        List<Movie> movies = movieService.getAllMovies();
        log.info("found {} movies: ", movies.size());

        List<MovieDto> mdtos = movies.stream()
                .map(movie -> MovieConverter.toDto(movie))
                .toList();

        return mdtos;
    }
    @GetMapping(value = "/movies/{movieId}")
    ResponseEntity<MovieDto> getMovie(@PathVariable("movieId") int movieId) {
        log.info("about to get movie {}", movieId);

        Optional<Movie> movie = Optional.ofNullable(movieService.getMovieById(movieId));
        if (movie.isPresent()) {
            MovieDto mdtos = MovieConverter.toDto(movie.get());
            return ResponseEntity.ok(mdtos);
        } else
            return ResponseEntity.notFound().build();

    }


    @GetMapping(value = "/cinemas/{cinemaId}/movies")
    ResponseEntity<List<MovieDto>> getCinemasShowingMovie(@PathVariable("cinemaId") int cinemaId) {

        log.info("about to retrieve movies showing in {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);

        if ( cinema != null ){
            List<Movie> movies = cinemaService.getMoviesInCinema(cinema);
            log.info(" there is {} movies showing in cinema {}", movies.size(), cinema.getName());

            if(movies != null ) {
                return ResponseEntity.ok(
                        movies.stream()
                                .map(MovieConverter::toDto)
                                .toList());
            }
        }
        return ResponseEntity.notFound().build();

    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(@Validated @RequestBody MovieDto movieDto, Errors errors, HttpServletRequest request){
        log.info("about to add new movie {}", movieDto);

        // TODO validation
        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);

            List<String> errorMessages = errors.getAllErrors().stream()
                    .map(oe->messageSource.getMessage(
                            oe.getCode(),
                            translateArgs(oe.getArguments(), locale),
                            "MISSING MESSAGE FOR CODE:" + oe.getCode(),
                            locale))
                    .toList();
            return ResponseEntity.badRequest().body(errorMessages);
        }

        // bug emulation
        if(movieDto.getTitle().equals("Boom")){
            throw new IllegalArgumentException("Boom");
        }

        Movie movie = MovieConverter.fromDto(movieDto);

        movie = movieService.addMovie(movie);

        movieDto = MovieConverter.toDto(movie);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{id}")
                .build(Map.of(
                        "id", movie.getId()
                ));

        return ResponseEntity
                .created(uri)
                .body(movieDto);
    }




    private Object[] translateArgs(Object[] args, Locale locale){
        if(args==null) return null;
        Object[] resultArgs = new Object[args.length];
        for(int i=0; i<args.length; i++){
            if(args[i] instanceof DefaultMessageSourceResolvable resolvable) {
                resultArgs[i] = messageSource.getMessage(
                        "arg." + resolvable.getCode(),
                        new Object[0],
                        resolvable.getCode(),
                        locale);
            } else {
                resultArgs[i] = args[i];
            }
        }
        return resultArgs;
    }

}