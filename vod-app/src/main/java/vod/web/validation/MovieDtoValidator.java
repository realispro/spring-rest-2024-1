package vod.web.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.service.MovieService;
import vod.dto.MovieDto;

@Component
@RequiredArgsConstructor
public class MovieDtoValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieDto dto = (MovieDto) target;

        if(movieService.getDirectorById(dto.getDirectorId())==null){
            errors.rejectValue("directorId", "error.director.missing");
        }
    }
}
